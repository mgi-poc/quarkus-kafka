package de.gacme.poc;

import de.gacme.types.Person;
import io.smallrye.reactive.messaging.kafka.api.IncomingKafkaRecordMetadata;
import org.apache.kafka.common.header.Header;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

@ApplicationScoped
public class PersonConsumer {

    private static final Logger LOG =
            LoggerFactory.getLogger(PersonConsumer.class);

    @Incoming("person-in")
    @Transactional
    public CompletionStage<Void> consume(Message<Person> person) {


        LOG.info("consumed {}", person.getPayload());

//        person.getMetadata().get(IncomingKafkaRecordMetadata.class).get().getHeaders().headers();


        Optional<String> eventName = person.getMetadata().get(IncomingKafkaRecordMetadata.class).map(IncomingKafkaRecordMetadata::getHeaders)
                .map(headers -> headers.headers("myEvent"))
                .map(Iterable::iterator)
                .filter(Iterator::hasNext)
                .map(Iterator::next)
                .map(Header::value)
                .map(String::new);

        LOG.info("eventName {}", eventName);

        if(person.getPayload().getAge() < 18) {

            LOG.warn("to young! nack {}", person.getPayload());
            return person.nack(new IllegalArgumentException("person to young"));
        }


        LOG.info("ack {}", person.getPayload());
        return person.ack();


    }

}