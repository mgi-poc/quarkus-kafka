package de.gacme.poc.serialization;

import de.gacme.types.Person;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;
import io.vertx.core.json.JsonObject;
import io.vertx.json.schema.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class PersonDeserializer extends ObjectMapperDeserializer<Person> {

    private static final Logger LOG =
            LoggerFactory.getLogger(PersonDeserializer.class);

    public PersonDeserializer() {
        super(Person.class);
    }

    @Override
    public Person deserialize(String topic, byte[] data) {

        String jsonSchema = readPersonSchema();
        LOG.info("schema {}", jsonSchema);
        Validator validator = createValidator(jsonSchema);

        LOG.info("deserialize[{}] {}\n" +
                "as base64 {}", topic, data, Base64.getEncoder().encodeToString(data));


        String json = new String(data, StandardCharsets.UTF_8);
        OutputUnit result = validator.validate(json);

        LOG.info("json {}", json);
        LOG.info("validation result {}", result);


        return super.deserialize(topic, data);
    }

    private Validator createValidator(String jsonSchema) {
        JsonSchema schema = JsonSchema.of(new JsonObject(jsonSchema));
        JsonSchemaOptions jsonSchemaOptions = new JsonSchemaOptions();
        jsonSchemaOptions.setDraft(Draft.DRAFT202012);
        jsonSchemaOptions.setBaseUri("https://gacme.de");

        return Validator.create(schema, jsonSchemaOptions);
    }

    private String readPersonSchema() {
        String jsonSchema;
        try {
            jsonSchema = IOUtils.toString(this.getClass()
                            .getResourceAsStream("/api/json-schema/person.schema.json"),
                    "UTF-8");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return jsonSchema;
    }
}
