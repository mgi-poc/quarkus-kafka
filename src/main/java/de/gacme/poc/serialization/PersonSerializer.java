package de.gacme.poc.serialization;

import de.gacme.types.Person;
import io.quarkus.kafka.client.serialization.ObjectMapperSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;

public class PersonSerializer extends ObjectMapperSerializer<Person> {
    private static final Logger LOG =
            LoggerFactory.getLogger(PersonSerializer.class);

    @Override
    public byte[] serialize(String topic, Person data) {
        byte[] bdata = super.serialize(topic, data);
        LOG.info("serialize[{}] {}\n" +
                "as base64 {}", topic, bdata, Base64.getEncoder().encodeToString(bdata));
        return bdata;
    }
}
