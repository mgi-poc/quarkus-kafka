package de.gacme.poc;

import de.gacme.types.Person;
import io.smallrye.reactive.messaging.kafka.api.OutgoingKafkaRecordMetadata;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.CompletableFuture;

@Path("/produce")
public class MyEmitterResource {

    private static final Logger LOG =
            LoggerFactory.getLogger(MyEmitterResource.class);

    @Inject
    @Channel("person-out")
    Emitter<Person> personEmitter;

    @GET
    @Path("/person")
    @Produces(MediaType.TEXT_PLAIN)
    public Person producePerson() {

        Person person = new Person();

        person.setFirstName(RandomStringUtils.randomAlphabetic(10));
        person.setLastName(RandomStringUtils.randomAlphabetic(10));
        person.setAge(RandomUtils.nextInt(0, 100));


        Message<Person> message = asMessage(Person.class, person);


        personEmitter.send(message);

        LOG.info("produced {}", person);

        return person;
    }

    private <T> Message<T> asMessage(Class<T> clazz, T data) {

        OutgoingKafkaRecordMetadata<String> metadata = OutgoingKafkaRecordMetadata.<String>builder()
                .withHeaders(new RecordHeaders().add("myEvent", clazz.getSimpleName().getBytes()))
                .build();


        Message<T> message = Message.of(data, Metadata.of(metadata))
                .withAck(() -> {

                    LOG.info("ack");

                    return CompletableFuture.completedFuture(null);
                })
                .withNack(throwable -> {

                    LOG.warn("nack");

                    return CompletableFuture.completedFuture(null);
                });


        return message;
    }

}